package com.domain.domain;

import com.domain.Enum.Genre;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/*
@Fetch(SUBSELECT)      Hibernate now initializes all authors collections for all loaded Book instances as soon as you force the initialization of one bids collection:
@BatchSize(size = 25)  So, when books has a size of 25, all books will be fetched with 1 SELECT instead of 25 SELECTs.

@Fetch(SELECT)         Now, when an Book is loaded, the authors have to be loaded as well:
em.find(Book.class, ITEM_ID);
from Book where ID = ?
from Authors where ID = ?
..........
*/

@Entity
@Table
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "book-map")
public class Book extends Persistent {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    private int price;

    private double rating;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER) //todo: have to refactor it must
    @JoinTable(name = "book_author",
            joinColumns = @JoinColumn(name = "book_id"),
            inverseJoinColumns = @JoinColumn(name = "author_id"))
    @OrderColumn(name = "idx")
//    @Fetch(SUBSELECT)
    private List<Author> authors;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "book_genre", joinColumns = @JoinColumn(name = "book_id"))
    @Column(name = "genre")
    private Set<Genre> genres;

    public Book() {
    }

    public Book(long id, List<Author> authors) {
        this.id = id;
        this.authors = authors;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public Set<Genre> getGenres() {
        return genres;
    }

    public void setGenres(Set<Genre> genres) {
        this.genres = genres;
    }
}
