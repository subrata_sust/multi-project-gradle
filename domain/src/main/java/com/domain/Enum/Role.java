package com.domain.Enum;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static com.domain.Enum.Category.ADMIN;
import static com.domain.Enum.Category.AUTHOR;
import static com.domain.Enum.Category.USER;
import static java.util.Arrays.*;

public enum Role {
    BOOK_SUMBIT(USER, AUTHOR),
    BOOK_DELETE(USER, AUTHOR),
    BOOK_APPROVE(USER, AUTHOR),

    ADMIN_SETUP(ADMIN),

    AUTHOR_SUBMIT(AUTHOR),
    AUTHOR_DELETE(AUTHOR),
    AUTHOR_APPROVE(AUTHOR);

    Category[] categories;

    Role(Category... categories) {
        this.categories = categories;
    }

    public List<Role> getRoles(Category category) {
        return stream(Role.values())
                .filter(role -> stream(role.categories)
                        .anyMatch(c -> c.equals(category)))
                .collect(Collectors.toList());
    }
}
