package com.domain.domain;

import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.FetchProfile;
import org.hibernate.annotations.FetchProfiles;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table
@NamedEntityGraphs({
        @NamedEntityGraph(
                name = "authorBooks",
                attributeNodes = {
                        @NamedAttributeNode("books")
                }
        )
})
@FetchProfiles({
        @FetchProfile(name = "authorBooks",
                fetchOverrides = {
                        @FetchProfile.FetchOverride(
                                entity = Author.class,
                                association = "books",
                                mode = FetchMode.JOIN
                        )
                })
})
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "author-map")
public class Author extends Persistent {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false, unique = true)
    private String fullName;

    @ManyToMany(mappedBy = "authors")
    private List<Book> books;

    @ElementCollection
    @CollectionTable(
            name = "book_image",
            joinColumns = @JoinColumn(name = "book_id"))
    @Column(name = "image")
    @OrderColumn(name = "idx")
    private List<String> images = new ArrayList<>();

    @Transient
    private Book book;

    public Author() {
        books = new ArrayList<>();
    }

    public Author(Login login) {
        this();
    }

    public Author(long id) {
        this();
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
}
