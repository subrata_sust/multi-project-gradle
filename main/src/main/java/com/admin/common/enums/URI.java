package com.admin.common.enums;

public enum URI {

    LOGIN("/main", "/login"),
    SIGN_UP("/main", "/signup"),
    USER_HOME("/main", "/home"),
    AUTHOR("/main", "/author"),
    BOOK("/main", "/book"),
    BOOK_CREATE("/main", "/book/create"),
    BOOK_RATING("/main", "/book/rating"),
    TWO_FA("/main", "/signup/2fa"),
    DONE("/main", "/done");

    private String context;
    private String uri;

    URI(String context, String uri) {
        this.context = context;
        this.uri = uri;
    }

    public String getUri() {
        return uri;
    }

    public String getUriWIthContext() {
        return context + uri;
    }

}

