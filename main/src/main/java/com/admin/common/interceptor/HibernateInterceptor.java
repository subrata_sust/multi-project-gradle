package com.admin.common.interceptor;

import com.domain.domain.Persistent;
import com.mchange.v1.util.ArrayUtils;
import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;

@Component
public class HibernateInterceptor extends EmptyInterceptor {

    private Logger logger = LoggerFactory.getLogger(HibernateInterceptor.class);


    @Override
    public boolean onSave(Object entity, Serializable id,
                          Object[] state, String[] propertyNames, Type[] types) {

        if (entity instanceof Persistent) {
            logger.info("On Save Interceptor");

            int createdIndex = ArrayUtils.indexOf(propertyNames, "created");
            state[createdIndex] = new Date();

            int updatedIndex = ArrayUtils.indexOf(propertyNames, "updated");
            state[updatedIndex] = new Date();

            return true;
        }

        return false;
    }

    @Override
    public boolean onFlushDirty(Object entity, Serializable id,
                                Object[] state, Object[] previousState,
                                String[] propertyNames, Type[] types) {

        if (entity instanceof Persistent) {
            logger.info("On Flush and Update Interceptor");

            int updatedIndex = ArrayUtils.indexOf(propertyNames, "updated");
            state[updatedIndex] = new Date();

            return true;
        }

        return true;
    }
}
