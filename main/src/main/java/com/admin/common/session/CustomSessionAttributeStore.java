package com.admin.common.session;

import com.admin.common.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.support.DefaultSessionAttributeStore;
import org.springframework.web.context.request.WebRequest;

import static com.admin.common.session.SessionAttributeController.COMMAND_ID;
import static org.springframework.web.context.request.RequestAttributes.SCOPE_REQUEST;

public class CustomSessionAttributeStore extends DefaultSessionAttributeStore {

    Logger logger = LoggerFactory.getLogger(CustomSessionAttributeStore.class);

    @Override
    protected String getAttributeNameInSession(WebRequest request, String attributeName) {

        logger.info("get session attribute name:");

        return Utils.getSessionAttributeName(attributeName, (String) request.getAttribute(COMMAND_ID, SCOPE_REQUEST));
    }
}
