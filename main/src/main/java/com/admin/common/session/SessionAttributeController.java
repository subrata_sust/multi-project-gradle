package com.admin.common.session;

import com.admin.common.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;

import static com.admin.common.utils.Utils.getRandomId;
import static com.admin.common.utils.Utils.getRequest;
import static org.springframework.http.HttpMethod.GET;

public class SessionAttributeController<T> {

    public static String COMMAND_ID = "commandId";

    Logger logger = LoggerFactory.getLogger(SessionAttributeController.class);

    @ModelAttribute
    public void createCommandId() {
        HttpServletRequest request = getRequest();

        if (GET.name().equals(request.getMethod())) {
            logger.info("from GET");
            request.setAttribute(COMMAND_ID, getCommandId(request));
        } else {
            logger.info("from POST");
            request.setAttribute(COMMAND_ID, ServletRequestUtils.getStringParameter(request, COMMAND_ID, ""));
        }
    }

    public String getCommandId(HttpServletRequest request) {
        String commandId = ServletRequestUtils.getStringParameter(request, COMMAND_ID, "");

        if (StringUtils.isEmpty(commandId)) {
            commandId = getRandomId();
        }

        return commandId;
    }

    public T getCommand(String commandName) {
        return (T) getRequest().getSession().getAttribute(Utils.getSessionAttributeName(commandName, getCommandId(getRequest())));
    }
}
