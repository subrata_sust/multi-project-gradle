package com.admin.common.controller;

import com.admin.common.command.DoneCmd;
import com.admin.common.enums.URI;
import com.admin.common.utils.StringUtils;
import com.admin.common.utils.UrlBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import static com.admin.common.utils.StringUtils.*;

@Controller
@RequestMapping("/done")
public class DoneController {

    public static final String COMMAND = "doneCmd";

    @GetMapping
    public String view(@ModelAttribute(COMMAND) DoneCmd doneCmd, ModelMap model) {

        if (isEmpty(doneCmd.getMessageKey())) {
            return "redirect:" + new UrlBuilder(URI.USER_HOME).getUrl();
        }

        model.put(COMMAND, doneCmd);

        return "done";
    }

}
