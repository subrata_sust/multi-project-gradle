package com.admin.common.resolver;

import com.admin.common.command.DoneCmd;
import com.admin.common.utils.UrlBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

import static com.admin.common.enums.TYPE.ERROR;
import static com.admin.common.enums.TYPE.SUCCESS;
import static com.admin.common.enums.URI.DONE;
import static com.admin.common.enums.URI.USER_HOME;
import static com.admin.common.utils.Utils.prepareDoneBean;

@ControllerAdvice
public class GlobalExceptionResolver {

    private Logger logger = LoggerFactory.getLogger(GlobalExceptionResolver.class);

    @ExceptionHandler(Exception.class)
    public String handleSQLException(HttpServletRequest request,
                                     Exception ex,
                                     RedirectAttributes redirectAttributes) {
        logger.info("Exception Occurred ::: " + ex);

        DoneCmd doneCmd = new DoneCmd("global.exception",
                ERROR, null, new UrlBuilder(USER_HOME).getUrlWithContext());

        prepareDoneBean(redirectAttributes, doneCmd);

        return "redirect:" + new UrlBuilder(DONE).getUrl();
    }

}
