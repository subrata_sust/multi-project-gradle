package com.admin.common.helper;

import com.admin.common.utils.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static com.admin.common.enums.URI.LOGIN;
import static com.admin.common.enums.URI.SIGN_UP;
import static com.admin.common.utils.StringUtils.*;

public class AuthHelper {

    public static String getAuthIdFromSession(HttpServletRequest request) {

        try {
            HttpSession session = request.getSession();
            return (String) session.getAttribute("authId");
        } catch (NullPointerException ignore) {
            return null;
        }
    }

    public static boolean isExcludedFromFilter(String path) {
        return path.contains(LOGIN.getUri()) || path.contains(SIGN_UP.getUri());
    }

    public static boolean isInvalidPath(String authId, String path) {
        return isNotEmpty(authId) &&
                (path.contains(LOGIN.getUri()) || path.contains(SIGN_UP.getUri()));
    }

}
