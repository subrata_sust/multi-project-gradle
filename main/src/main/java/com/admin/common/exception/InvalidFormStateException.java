package com.admin.common.exception;

public class InvalidFormStateException extends RuntimeException {

    public static final String COMMON_MSG = "Invalid Form State Found";

    public String msg;

    public InvalidFormStateException() {
        this.msg = COMMON_MSG;
    }

    public InvalidFormStateException(String msg) {
        this.msg = msg;
    }
}
