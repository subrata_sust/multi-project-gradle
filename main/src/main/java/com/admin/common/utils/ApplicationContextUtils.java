package com.admin.common.utils;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.*;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/*
Spring instantiates the bean.

Spring injects values and bean references into the bean’s properties.
If the bean implements BeanNameAware , Spring passes the bean’s ID to the set-
BeanName() method.

If the bean implements BeanFactoryAware , Spring calls the setBeanFactory()
method, passing in the bean factory itself.

If the bean implements ApplicationContextAware , Spring calls the set-
ApplicationContext() method, passing in a reference to the enclosing appli-
cation context.

If the bean implements the BeanPostProcessor interface, Spring calls its post-
ProcessBeforeInitialization() method.

If the bean implements the InitializingBean interface, Spring calls its after-
PropertiesSet() method. Similarly, if the bean was declared with an init-
method , then the specified initialization method is called.

If the bean implements BeanPostProcessor , Spring calls its postProcess-
AfterInitialization() method.

At this point, the bean is ready to be used by the application and remains in the
application context until the application context is destroyed.

If the bean implements the DisposableBean interface, Spring calls its
destroy() method. Likewise, if the bean was declared with a destroy-method ,
the specified method is called.
* */
@Component
public class ApplicationContextUtils implements BeanNameAware, BeanFactoryAware, ApplicationContextAware,
        BeanPostProcessor, InitializingBean, DisposableBean {

    private BeanFactory beanFactory;
    private String beanName;
    private ApplicationContext applicationContext;


    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }

    @Override
    public void setBeanName(String name) {
        this.beanName = name;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Override
    public void afterPropertiesSet() throws Exception {

    }

    @Override
    public void destroy() throws Exception {

    }

    public BeanFactory getBeanFactory() {
        return beanFactory;
    }

    public String getBeanName() {
        return beanName;
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }
}
