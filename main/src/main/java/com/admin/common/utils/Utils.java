package com.admin.common.utils;

import com.admin.common.command.DoneCmd;
import com.admin.common.controller.DoneController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

import static com.admin.common.controller.DoneController.*;

public class Utils {

    public static HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }

    public static String getRandomId() { /* simple random number generator. Have to rewrite */
        return System.currentTimeMillis() % 777 + "-" + System.currentTimeMillis() % 917;
    }

    public static String getSessionAttributeName(String commandName, String commandId) {
        return commandName + "-" + commandId;
    }

    public static void prepareDoneBean(RedirectAttributes redirectAttributes, DoneCmd doneCmd) {
        redirectAttributes.addFlashAttribute(COMMAND, doneCmd);
    }

}
