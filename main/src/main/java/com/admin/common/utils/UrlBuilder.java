package com.admin.common.utils;

import com.admin.common.enums.URI;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.Map;

public class UrlBuilder {

    private Map<String, Object> map = new HashMap();
    private URI uri;

    public UrlBuilder(URI uri) {
        this.uri = uri;
    }

    public UrlBuilder addParam(String key, Object value) {
        map.put(key, value);

        return this;
    }

    public String getUrl() {
        return buildUrl(uri.getUri());
    }

    public String getUrlWithContext() {
        return buildUrl(uri.getUriWIthContext());
    }

    private String buildUrl(String url) {

        if (!CollectionUtils.isEmpty(map)) {
            url += "?";
        }

        for (String key : map.keySet()) {
            if (!url.endsWith("?")) {
                url += "&";
            }

            url += key + "=" + map.get(key);
        }

        return url;
    }
}
