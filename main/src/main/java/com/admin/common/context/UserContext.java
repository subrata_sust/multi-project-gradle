package com.admin.common.context;

import com.admin.common.utils.Utils;
import com.domain.domain.Login;
import com.hazelcast.config.ClasspathXmlConfig;
import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;

import java.util.concurrent.ConcurrentHashMap;

import static com.admin.common.utils.EncryptDecryptUtils.encrypt;
import static com.admin.common.utils.Utils.*;

public class UserContext {

    public static IMap<Object, Object> loginList;

    public static HazelcastInstance hazelcastInstance;

    static {
        Config config= new ClasspathXmlConfig("hazelcast.xml");
        config.setInstanceName("hazelcastInstance");

        hazelcastInstance = Hazelcast.newHazelcastInstance(config);

        loginList = hazelcastInstance.getMap("userContextMap");
    }

    public static Login getLogin() {
        return (Login) loginList.get(getRequest().getSession().getAttribute("authId"));
    }

    public static void saveLogin(Login login) {
        String encryptedId = encrypt(String.valueOf(login.getId()));

        getRequest().getSession().setAttribute("authId", encryptedId);

        loginList.putIfAbsent(encryptedId, login);
    }
}
