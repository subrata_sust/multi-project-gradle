package com.admin.editor;

import com.admin.service.AuthorService;
import com.domain.domain.Author;
import org.springframework.util.StringUtils;

import java.beans.PropertyEditorSupport;


public class AuthorEditor extends PropertyEditorSupport {

    private AuthorService authorService;

    public AuthorEditor(AuthorService authorService) {
        this.authorService = authorService;
    }

    @Override
    public void setAsText(String text) {
        if (StringUtils.hasText(text)) {
            long value = Long.parseLong(text);
            setValue(authorService.getReference(value));
        } else {
            setValue(null);
        }
    }
}
