package com.admin.filter;

import com.admin.common.context.AccessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.admin.common.enums.URI.*;
import static com.admin.common.helper.AuthHelper.*;
import static com.admin.common.utils.StringUtils.*;

@WebFilter(value = "/*")
public class AuthFilter implements Filter {

    private Logger logger = LoggerFactory.getLogger(AuthFilter.class);

    @Autowired
    private AccessContext accessContext;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest servletRequest = (HttpServletRequest) request;
        HttpServletResponse servletResponse = (HttpServletResponse) response;
        String servletPath = servletRequest.getServletPath();

        addLogInfo(servletRequest);

        String authId = getAuthIdFromSession(servletRequest);

        if (isInvalidPath(authId, servletPath)) {  // example: login/signup
            servletResponse.sendRedirect(USER_HOME.getUriWIthContext());
            return;
        }

        if (isExcludedFromFilter(servletRequest.getServletPath())) {
            chain.doFilter(request, response);
            return;
        }

        if (isEmpty(authId)) {
            servletResponse.sendRedirect(LOGIN.getUriWIthContext());
            return;
        }

        if (true) {   //todo:: have to add role must
            chain.doFilter(request, response);
        }
    }

    private void addLogInfo(HttpServletRequest request) {
        MDC.put("ip", request.getRemoteAddr());
        MDC.put("method", request.getMethod());
        MDC.put("uri", String.valueOf(request.getRequestURL()));
    }

    @Override
    public void destroy() {

    }
}
