package com.admin.validator;

import com.admin.service.LoginService;
import com.domain.domain.Login;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class SignUpValidator implements Validator {

    @Autowired
    private LoginService loginService;

    @Override
    public boolean supports(Class<?> clazz) {
        return Login.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {

        Login login = (Login) target;

        if (loginService.exists(login.getEmail())) {
            errors.rejectValue("email", "duplicate.email");
        }

        if (!login.getPassword().equals(login.getConfirmPassword())) {
            errors.rejectValue("confirmPassword", "password.mismatch");
        }
    }
}
