package com.admin.validator;

import com.domain.domain.Login;
import com.warrenstrange.googleauth.GoogleAuthenticator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class TwoFAValidator implements Validator {

    @Autowired
    private GoogleAuthenticator googleAuthenticator;

    @Override
    public boolean supports(Class<?> clazz) {
        return Login.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Login login = (Login) target;

        boolean authorized = googleAuthenticator.authorize(login.getSecretKey(), login.getVerificationCode());

        if (login.isEnabled2FA() && !authorized) {
            errors.rejectValue("verificationCode", "empty.verification.code");
        }

    }
}
