package com.admin.service;

import com.admin.common.exception.InvalidFormStateException;
import com.domain.domain.Author;
import com.domain.domain.Login;
import org.hibernate.Hibernate;
import org.hibernate.NonUniqueObjectException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

@Repository
public class LoginService {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void save(Login login) {
        entityManager.persist(login);
    }

    @Transactional
    public void update(Login login) {
        entityManager.merge(login);
    }

    @Transactional
    public Login get(long id) {
        return entityManager.find(Login.class, id);
    }

    @Transactional
    public Login get(String email, String password) {
        try {
            return entityManager.createQuery("FROM Login WHERE email=:email AND password=:password", Login.class)
                    .setParameter("email", email)
                    .setParameter("password", password)
                    .getSingleResult();
        } catch (NoResultException exception) {
            return null;
        } catch (NonUniqueObjectException exception) {
            throw new InvalidFormStateException();
        }
    }

    @Transactional
    public boolean exists(String email) {
        return entityManager.createQuery("From Login Where email = :email")
                .setParameter("email", email)
                .getResultList()
                .size() > 0;
    }
}
