package com.admin.service;

import com.domain.domain.Author;
import com.domain.domain.Book;
import com.domain.domain.Login;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class BookService {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private AuthorService authorService;

    public Book get(long id) {
        return entityManager.find(Book.class, id);
    }

    @Transactional
    public void save(Book book) {
        entityManager.persist(book);
    }

    @Transactional
    public void update(Book book) {
        entityManager.merge(book);
    }


}
