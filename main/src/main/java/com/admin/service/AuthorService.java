package com.admin.service;

import com.domain.domain.Author;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;

@Repository
public class AuthorService {

    @PersistenceContext
    private EntityManager entityManager;

    public Author getReference(long id) {
        return entityManager.getReference(Author.class, id);
    }

    public Author get(long id) {

        /**
         * enabling entityGraph to load lazy association
         */

        /**
         *Map<String, Object> properties = new HashMap<>();
         *properties.put("javax.persistence.loadgraph", entityManager.getEntityGraph("authorBooks"));
         *return entityManager.find(Author.class, id, properties);
         * /

         /**
         * enabling fetch profile
         */
        entityManager.unwrap(Session.class).enableFetchProfile("authorBooks");

        return entityManager.find(Author.class, id);
    }

    @Transactional
    public void save(Author author) {
        entityManager.persist(author);
    }

    @Transactional
    public void update(Author author) {
        author = entityManager.merge(author);
    }

    public List<Author> getAll() {
        return entityManager.createQuery("FROM Author")
                .getResultList();
    }
}
