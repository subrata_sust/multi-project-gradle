package com.admin.controller;

import com.admin.common.command.DoneCmd;
import com.admin.common.session.SessionAttributeController;
import com.admin.common.utils.UrlBuilder;
import com.admin.service.AuthorService;
import com.admin.service.BookService;
import com.domain.domain.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

import static com.admin.common.enums.TYPE.SUCCESS;
import static com.admin.common.enums.URI.*;
import static com.admin.common.utils.Utils.prepareDoneBean;

@Controller
@SessionAttributes(AuthorController.COMMAND_NAME)
public class AuthorController extends SessionAttributeController<Author> {

    public static final String COMMAND_NAME = "author";

    @Autowired
    private BookService loginService;

    @Autowired
    private AuthorService authorService;

    @GetMapping("/author")
    public String showAuthor(@RequestParam long id, ModelMap modelMap) {
        Author author = authorService.get(id);

        modelMap.put(COMMAND_NAME, author);

        setReferenceData(modelMap);

        return "author";
    }

    @GetMapping("/author/create")
    public String showCreateAuthorForm(ModelMap modelMap) {

        modelMap.put(COMMAND_NAME, new Author());

        return "createAuthor";
    }

    @PostMapping("/author/create")
    public String submitAuthor(@Valid @ModelAttribute(COMMAND_NAME) Author author,
                               BindingResult result,
                               SessionStatus sessionStatus,
                               RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            return "author";
        }

        authorService.save(author);

        sessionStatus.setComplete();

        DoneCmd doneCmd = new DoneCmd("success.create.author", SUCCESS, null,
                new UrlBuilder(AUTHOR)
                        .addParam("id", author.getId())
                        .getUrlWithContext());

        prepareDoneBean(redirectAttributes, doneCmd);

        return "redirect:" + new UrlBuilder(DONE).getUrl();
    }

    private void setReferenceData(ModelMap modelMap) {
        modelMap.put("optionMap", getOptionMap());
    }

    public Map<String, String> getOptionMap() {
        Map<String, String> optionMap = new HashMap<>();

        optionMap.put("Add Book", new UrlBuilder(BOOK_CREATE).getUrlWithContext());

        return optionMap;
    }
}
