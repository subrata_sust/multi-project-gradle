package com.admin.controller;

import com.admin.common.session.SessionAttributeController;
import com.admin.common.utils.UrlBuilder;
import com.admin.service.LoginService;
import com.admin.validator.SignUpValidator;
import com.domain.domain.Login;
import com.warrenstrange.googleauth.GoogleAuthenticator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import static com.admin.common.enums.URI.*;
import static java.lang.String.valueOf;

@Controller
@SessionAttributes(SignUpController.COMMAND_NAME)
@RequestMapping("/signup")
public class SignUpController extends SessionAttributeController<Login> {

    private Logger logger = LoggerFactory.getLogger(SignUpController.class);

    public static final String COMMAND_NAME = "login";

    @Autowired
    private LoginService loginService;

    @Autowired
    private GoogleAuthenticator googleAuthenticator;

    @Autowired
    private SignUpValidator signUpValidator;

    @InitBinder(COMMAND_NAME)
    public void init(WebDataBinder binder) {
        binder.addValidators(signUpValidator);
    }

    @GetMapping
    public String showForm(ModelMap modelMap) {

        modelMap.put(COMMAND_NAME, new Login());

        return "signUp";
    }

    @PostMapping
    public String showForm(@Valid @ModelAttribute(COMMAND_NAME) Login login,
                           BindingResult result,
                           HttpServletRequest request,
                           SessionStatus sessionStatus) {

        if (result.hasErrors()) {
            return "signUp";
        }

        loginService.save(login);

        sessionStatus.setComplete();

        return "redirect:" + new UrlBuilder(LOGIN).getUrl();
    }
}
