package com.admin.controller;

import com.admin.common.command.DoneCmd;
import com.admin.common.enums.URI;
import com.admin.common.utils.UrlBuilder;
import com.admin.common.session.SessionAttributeController;
import com.admin.editor.AuthorEditor;
import com.admin.service.AuthorService;
import com.admin.service.BookService;
import com.domain.Enum.Genre;
import com.domain.domain.Author;
import com.domain.domain.Book;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static com.admin.common.enums.TYPE.SUCCESS;
import static com.admin.common.enums.URI.*;
import static com.admin.common.utils.Utils.prepareDoneBean;

@Controller
@SessionAttributes(BookController.COMMAND_NAME)
@RequestMapping("/book")
public class BookController extends SessionAttributeController<Book> {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookController.class);

    public static final String COMMAND_NAME = "book";

    @Autowired
    private BookService bookService;

    @Autowired
    private AuthorService authorService;

    @InitBinder(COMMAND_NAME)
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Author.class, new AuthorEditor(authorService));
    }

    @GetMapping
    public String showBook(ModelMap model, @RequestParam long id, HttpServletRequest request) {
        Book book = bookService.get(id);
        model.put(COMMAND_NAME, book);

        setReferenceData(model, book);

        return "book";
    }

    @PostMapping
    public String updateBook(@Valid @ModelAttribute(COMMAND_NAME) Book book,
                             SessionStatus sessionStatus,
                             RedirectAttributes redirectAttributes) {
        bookService.update(book);

        sessionStatus.setComplete();

        DoneCmd doneCmd = new DoneCmd("success.update.book", SUCCESS, null,
                new UrlBuilder(BOOK)
                        .addParam("id", book.getId())
                        .getUrlWithContext());

        prepareDoneBean(redirectAttributes, doneCmd);

        return "redirect:" + new UrlBuilder(DONE).getUrl();
    }

    @GetMapping("/create")
    public String showForm(ModelMap model) {
        Book book = new Book();
        model.put(COMMAND_NAME, book);

        setReferenceData(model, book);

        return "createBook";
    }

    @PostMapping("/create")
    public String submitBook(@Valid @ModelAttribute(COMMAND_NAME) Book book,
                             SessionStatus sessionStatus,
                             RedirectAttributes redirectAttributes) {
        bookService.save(book);

        sessionStatus.setComplete();

        DoneCmd doneCmd = new DoneCmd("success.create.book", SUCCESS, null,
                new UrlBuilder(BOOK)
                        .addParam("id", book.getId())
                        .getUrlWithContext());

        prepareDoneBean(redirectAttributes, doneCmd);

        return "redirect:" + new UrlBuilder(DONE).getUrl();
    }

    private void setReferenceData(ModelMap model, Book book) {
        model.put("optionMap", getOptionMap(book));
        model.put("tempAuthors", authorService.getAll());
        model.put("tempGenres", Arrays.asList(Genre.values()));
    }

    private Map<String, Object> getOptionMap(Book book) {
        Map<String, Object> optionMap = new HashMap<>();

        optionMap.put("Add Rating", new UrlBuilder(BOOK_RATING)
                .addParam("id", book.getId())
                .getUrlWithContext());

        return optionMap;
    }
}
