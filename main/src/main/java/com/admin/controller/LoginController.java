package com.admin.controller;


import com.admin.common.context.UserContext;
import com.admin.common.session.SessionAttributeController;
import com.admin.common.utils.UrlBuilder;
import com.admin.service.LoginService;
import com.domain.domain.Login;
import com.warrenstrange.googleauth.GoogleAuthenticator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.servlet.http.HttpServletRequest;

import static com.admin.common.enums.URI.*;
import static com.admin.controller.LoginController.COMMAND_NAME;

@Controller
@SessionAttributes(COMMAND_NAME)
public class LoginController extends SessionAttributeController<Login> {

    public static final String COMMAND_NAME = "login";

    @Autowired
    private LoginService loginService;

    @Autowired
    private GoogleAuthenticator googleAuthenticator;

    @GetMapping("/login")
    public String showForm(ModelMap modelMap) {

        modelMap.put(COMMAND_NAME, new Login());

        return "login";
    }

    @PostMapping("/login")
    public String signIn(@ModelAttribute Login login,  // TODO::have to add validation
                         HttpServletRequest request,
                         SessionStatus sessionStatus) {

        Login dbLogin = loginService.get(login.getEmail(), login.getPassword());

        if (dbLogin == null) {
            return "redirect:" + new UrlBuilder(LOGIN).getUrl();
        }

        if (dbLogin != null && dbLogin.isEnabled2FA() &&
                !googleAuthenticator.authorize(dbLogin.getSecretKey(), login.getVerificationCode())) {

            return "verificationCode";
        }

        UserContext.saveLogin(dbLogin);  /*TODO:: Add the dbLogin in hazlecast map instead of concurrentmap*/

        sessionStatus.setComplete();

        return "redirect:" + new UrlBuilder(USER_HOME)
                .getUrl();
    }

    @GetMapping(value = "/logout")
    public String logout(HttpServletRequest request) {

        request.getSession().invalidate();

        return "redirect:" + new UrlBuilder(LOGIN)
                .getUrl();
    }
}
