package com.admin.controller;

import com.admin.common.context.UserContext;
import com.admin.common.command.DoneCmd;
import com.admin.common.utils.UrlBuilder;
import com.admin.service.LoginService;
import com.admin.validator.TwoFAValidator;
import com.domain.domain.Author;
import com.domain.domain.Login;
import com.warrenstrange.googleauth.GoogleAuthenticator;
import com.warrenstrange.googleauth.GoogleAuthenticatorKey;
import com.warrenstrange.googleauth.GoogleAuthenticatorQRGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

import static com.admin.common.enums.TYPE.*;
import static com.admin.common.enums.URI.*;
import static com.admin.common.utils.Utils.prepareDoneBean;

@Controller
@SessionAttributes(TwoFAController.COMMAND_NAME)
public class TwoFAController {

    private Logger logger = LoggerFactory.getLogger(TwoFAController.class);

    public static final String COMMAND_NAME = "login";

    @Autowired
    private GoogleAuthenticator googleAuthenticator;

    @Autowired
    private LoginService loginService;

    @Autowired
    private TwoFAValidator twoFAValidator;

    @InitBinder(COMMAND_NAME)
    public void init(WebDataBinder binder) {
        binder.addValidators(twoFAValidator);
    }

    @GetMapping("/2fa")
    public String show2FAForm(ModelMap modelMap) {

        Login login = loginService.get(UserContext.getLogin().getId());

        modelMap.put(COMMAND_NAME, login);

        return "2fa";
    }

    @PostMapping("/2fa")
    public String submit2FaForm(@Valid @ModelAttribute(COMMAND_NAME) Login login,
                                BindingResult result,
                                ModelMap modelMap,
                                SessionStatus sessionStatus,
                                RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            return "2fa";
        }

        loginService.update(login);

        sessionStatus.setComplete();

        DoneCmd doneCmd = new DoneCmd(login.isEnabled2FA() ? "success.2fa" : "disabled.2fa",
                SUCCESS, null, new UrlBuilder(USER_HOME).getUrlWithContext());

        prepareDoneBean(redirectAttributes, doneCmd);

        return "redirect:" + new UrlBuilder(DONE).getUrl();
    }

    @GetMapping("/qr")
    public @ResponseBody
    String enable2FA(@ModelAttribute(COMMAND_NAME) Login login) {

        return setAndGetQrCodeUrl(login);
    }

    public String setAndGetQrCodeUrl(Login login) {

        GoogleAuthenticatorKey googleAuthenticatorKey = googleAuthenticator.createCredentials();

        login.setSecretKey(googleAuthenticatorKey.getKey());

        login.setQrCodeUrl(GoogleAuthenticatorQRGenerator.getOtpAuthURL(
                "Multi Module Project", login.getEmail(), googleAuthenticatorKey));

        return login.getQrCodeUrl();
    }
}
