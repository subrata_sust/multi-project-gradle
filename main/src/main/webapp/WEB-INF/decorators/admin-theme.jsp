<?xml version="1.0" encoding="UTF-8" ?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Multi Module Project :: </title>
    <decorator:title/>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="<c:url value="/resources/css/user-style.css" />" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>

<nav class="navbar navbar-inverse pull-center">
    <div class="sidebar-nav">
        <ul class="navbar-header">
            <a class="navbar-brand" href="/main/home">Multi-Module Project</a>
        </ul>
        <ul class="nav navbar-nav navbar-center">
            <c:if test="${not empty authId}">
                <li><a href="/main/home">Home</a></li>
                <li><a href="/main/2fa">Enable 2FA</a></li>
                <li><a href="/main/author/create">Create Author</a></li>
                <li><a href="/main/book/create">Create Book</a></li>
            </c:if>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <c:choose>
                <c:when test="${empty authId}">
                    <li><a href="/main/signup"><span class="glyphicon glyphicon-user"></span>Sign Up</a></li>
                    <li><a href="/main/login"><span class="glyphicon glyphicon-log-in"></span> Sign In</a></li>
                </c:when>
                <c:otherwise>
                    <li><a href="/main/logout"><span class="glyphicon glyphicon-log-out"></span> Log Out</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
        <c:if test="${not empty authId}">
            <form class="navbar-form navbar-left">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Search</button>
            </form>
        </c:if>
    </div>
</nav>

<div class="container">
    <div class="row">
        <div class="col-md-2">
            <c:if test="${not empty authId}">
                <div class="sidebar-nav">
                    <div class="navbar navbar-default" role="navigation">
                        <div class="navbar-collapse collapse sidebar-navbar-collapse">
                            <ul class="nav navbar-nav">
                                <li><a href="#">Menu Item 1</a></li>
                                <li><a href="#">Menu Item 2</a></li>
                                <li><a href="#">Menu Item 3</a></li>
                                <li><a href="#">Menu Item 4</a></li>
                                <li><a href="#">Reviews</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </c:if>
        </div>
        <div class="col-md-10">
            <decorator:body/>
        </div>
    </div>
</div>

<div class="footer">
    <div class="container">
        <div class="col-md-1"></div>
        <div class="col-md-6">
            <p class="text-muted">2018 Multi-Module Project. All rights reserved</p>
        </div>
    </div>
</div>
<script src="<c:url value="/resources/lib/jquery-3.1.1.min.js" />"></script>
<script src="<c:url value="/resources/lib/bootstrap/js/bootstrap.min.js" />"></script>
</body>
</html>
