<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="error-template">
                <h1>Oops!</h1>
                <h2>404 Not Found</h2>
                <br>
                <div class="error-actions">
                    <a href="/main/home" class="btn btn-primary btn-lg">
                        <span class="glyphicon glyphicon-home"></span>
                        Take Me Home
                    </a>
                    <a href="/main/help" class="btn btn-default btn-lg">
                        <span class="glyphicon glyphicon-envelope"></span>
                        Contact Support
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
