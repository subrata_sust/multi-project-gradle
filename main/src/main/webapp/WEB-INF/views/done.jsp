<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<body>
<form:form modelAttribute="doneCmd">
    <div class="container">
        <div class="col-md-6 login-area shadow-depth-1">
            <fieldset>
                <c:choose>
                    <c:when test="${doneCmd.type == 'SUCCESS'}">
                        <legend>Action Completed</legend>
                    </c:when>
                    <c:otherwise>
                        <legend>Action Failure</legend>
                    </c:otherwise>
                </c:choose>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <c:if test="${not empty doneCmd.messageKey }">
                                <fmt:message code="${doneCmd.messageKey}"/>
                            </c:if>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6 pull-left">
                            <c:if test="${not empty doneCmd.backLink}">
                                <a href="${doneCmd.backLink}" class="btn btn-info">
                                    Back
                                </a>
                            </c:if>
                        </div>
                        <div class="col-md-6 pull-right">
                            <c:if test="${not empty doneCmd.nextUrl}">
                                <a href="${doneCmd.nextUrl}" class="btn btn-info">
                                    Next
                                </a>
                            </c:if>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</form:form>
</body>
</html>
