<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="b" uri="/WEB-INF/tld/customTagLibrary.tld" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<body>
<form:form modelAttribute="book" method="post">

    <input name="commandId" value="${commandId}" hidden/>

    <div class="container">
        <div class="col-md-6 login-area shadow-depth-1">
            <fieldset>
                <!-- Form Name -->
                <legend>Create Book</legend>

                <div class="form-group">

                    <div class="row">
                        <div class="col-md-4">
                            <label class="pull-right">
                                Book Name
                            </label>
                        </div>

                        <div class="col-md-6">
                            <form:input path="name" id="name" name="name"
                                        type="text"
                                        placeholder="Book Name"
                                        class="form-control input-md"
                                        required="required"/>
                            <form:errors path="name" class="text-danger"/>
                        </div>
                    </div>

                    <b:inputMultiSelect labelValue="Genres"
                                        bindPath="genres"
                                        optionList="${tempGenres}"/>

                    <b:inputMultiSelect labelValue="Authors"
                                        bindPath="authors"
                                        itemLabel="fullName"
                                        itemValue="id"
                                        optionList="${tempAuthors}"/>

                    <div class="row">
                        <div class="col-md-4">
                            <label class="pull-right">
                                Price
                            </label>
                        </div>

                        <div class="col-md-6">
                            <form:input path="price" id="price" name="price"
                                        type="text"
                                        placeholder="Book Price"
                                        class="form-control input-md"
                                        required="required"/>
                            <form:errors path="price" class="text-danger"/>
                        </div>
                    </div>
                </div>

                <div class=" form-group">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <input type="submit" class="btn btn-info" value="Submit">
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</form:form>
</div>
</body>
