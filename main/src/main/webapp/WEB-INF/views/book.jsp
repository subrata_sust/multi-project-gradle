<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="b" uri="/WEB-INF/tld/customTagLibrary.tld" %>
<body>

<form:form modelAttribute="book" method="post">
    <input name="commandId" value="${commandId}" hidden/>

    <div class="container">
        <div class="col-md-6 login-area shadow-depth-1">
            <fieldset>
                <!-- Form Name -->
                <legend>Author Definition</legend>

                <!-- Text input-->
                <div class="form-group">
                    <b:labelValue labelValue="Book Name"
                                  value="${book.name}"/>

                    <b:labelValue labelValue="Price"
                                  value="${book.price}"/>

                    <b:labelValue labelValue="Rating"
                                  value="${book.rating}"/>

                    <b:inputMultiSelect labelValue="Genres"
                                        bindPath="genres"
                                        optionList="${tempGenres}"/>

                    <b:inputMultiSelect labelValue="Authors"
                                        bindPath="authors"
                                        itemLabel="fullName"
                                        itemValue="id"
                                        optionList="${tempAuthors}"/>

                    <b:labelValue labelValue="Credits"
                                  value="Thanks to GOD"/>

                </div>

                <div class=" form-group">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <input type="submit" class="btn btn-info" value="Update">
                        </div>
                    </div>
                </div>

            </fieldset>
        </div>
    </div>
</form:form>
</body>
