<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<body>
<div class="container">
    <form:form modelAttribute="login" method="post">

        <input name="commandId" value="${commandId}" hidden/>

        <div class="col-md-6 login-area shadow-depth-1">
            <fieldset>
                <!-- Form Name -->
                <legend>2-Step Verification</legend>
                <h5>
                    This extra step shows it's really you trying to sign in.
                    USe your Google Authentication App to provide Verification Code
                </h5>

                <!-- Text input-->
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-4 control-label" for="verificationCode">
                            Verification Code
                        </label>

                        <div class="col-md-8">
                            <form:input path="verificationCode"
                                        id="verificationCode"
                                        name="verificationCode"
                                        type="text"
                                        placeholder="code"
                                        class="form-control input-md" required="required"/>

                            <form:errors path="verificationCode" class="text-danger"/>
                        </div>
                    </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <input type="submit" class="btn btn-info" value="Next">
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </form:form>

</div>
</body>

