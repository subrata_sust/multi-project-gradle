<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<body>
<div class="container">
    <form:form modelAttribute="login" method="post">

        <input name="commandId" value="${commandId}" hidden/>

        <div class="col-md-6 login-area shadow-depth-1">
            <fieldset>
                <!-- Form Name -->
                <legend>SIGN UP</legend>

                <!-- Text input-->
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-4 control-label" for="email">
                            Email
                        </label>

                        <div class="col-md-8">
                            <form:input path="email"
                                        id="email"
                                        name="email"
                                        type="email"
                                        placeholder="your email"
                                        class="form-control input-md"
                                        required="required"/>
                            <form:errors path="email" class="text-danger"/>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-md-4 control-label" for="name">
                            Name
                        </label>

                        <div class="col-md-8">
                            <form:input path="name" id="name"
                                        name="name"
                                        type="text"
                                        placeholder="your name"
                                        class="form-control input-md"
                                        required="required"/>
                            <form:errors path="name" class="text-danger"/>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-md-4 control-label" for="password">
                            Password
                        </label>

                        <div class="col-md-8">
                            <form:input path="password"
                                        id="password"
                                        name="password"
                                        type="password"
                                        placeholder="your password"
                                        class="form-control input-md"
                                        required="required"/>
                            <form:errors path="password" class="text-danger"/>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-md-4 control-label" for="confirmPassword">
                            Confirm Password
                        </label>

                        <div class="col-md-8">
                            <form:input path="confirmPassword"
                                        id="confirmPassword"
                                        name="confirmPassword"
                                        type="password"
                                        placeholder="confirm password"
                                        class="form-control input-md" required="required"/>
                            <form:errors path="confirmPassword" class="text-danger"/>
                        </div>
                    </div>
                </div>

                <!-- Button -->
                <div class=" form-group">
                    <div class="row">
                        <div class="col-md-8">
                            <input type="submit" class="btn btn-info" value="Submit">
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </form:form>
</div>
</body>

