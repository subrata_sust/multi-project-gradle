<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<body>

<div class="container">
    <form:form modelAttribute="author" method="post">

        <input name="commandId" value="${commandId}" hidden/>

        <div class="col-md-6 login-area shadow-depth-1">
            <fieldset>
                <!-- Form Name -->
                <legend>Create Author</legend>

                <!-- Text input-->
                <div class="form-group">

                    <div class="row">
                        <label class="col-md-4 control-label" for="fullName">
                            Full Name
                        </label>

                        <div class="col-md-8">
                            <form:input path="fullName" id="fullName" name="fullName" type="text"
                                        placeholder="your full name"
                                        class="form-control input-md" required="required"/>
                            <form:errors path="fullName" class="text-danger"/>
                        </div>
                    </div>

                    <!-- Button -->
                    <div class=" form-group">
                        <div class="row">
                            <div class="col-md-8">
                                <input type="submit" class="btn btn-info" value="Submit">
                            </div>
                        </div>
                    </div>
            </fieldset>
        </div>
    </form:form>
</div>
</body>

