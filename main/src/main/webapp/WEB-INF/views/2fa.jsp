<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="b" uri="/WEB-INF/tld/customTagLibrary.tld" %>
<body>
<div class="container">
    <form:form modelAttribute="login" method="post">
        <input name="commandId" value="${commandId}" hidden/>

        <div class="col-md-6 login-area shadow-depth-1">
            <fieldset>
                <!-- Form Name -->
                <legend>2 Factor Authentication</legend>

                <!-- Text input-->
                <div class="form-group">
                    <b:labelValue labelValue="Email"
                                  value="${login.email}"/>

                    <b:labelValue labelValue="Name"
                                  value="${login.name}"/>

                    <div class="row">
                        <div class="col-md-4">
                            <label class="pull-right">
                                Enable 2FA
                            </label>
                        </div>

                        <div class="col-md-8">
                            <form:checkbox path="enabled2FA" id="enabled2FA" name="enabled2FA"/>
                            <form:errors path="enabled2FA" class="text-danger"/>
                        </div>
                    </div>

                    <c:set var="hidden" value="${empty login.qrCodeUrl}"/>

                    <div class="row qrCodeRow ${hidden? 'hidden':''}">
                        <div class="col-md-4">
                            <label class="pull-right">
                                Scan QR Code <br> (Use Google Authenticator App)
                            </label>
                        </div>

                        <div class="col-md-8">
                            <img id="qrCode" src="${login.qrCodeUrl}"/>
                        </div>
                    </div>

                    <div class="row qrCodeRow ${hidden? 'hidden':''}">
                        <label class="col-md-4 control-label">
                            Verification Code
                        </label>

                        <div class="col-md-8">
                            <form:input path="verificationCode"
                                        id="verificationCode"
                                        name="verificationCode"/>
                            <form:errors path="verificationCode" class="text-danger"/>
                        </div>
                    </div>

                </div>

                <!-- Button -->
                <div class=" form-group">
                    <div class="row">
                        <div class="col-md-8 text-center">
                            <input type="submit" class="btn btn-info" value="Confirm">
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </form:form>
</div>

<script type="text/javascript">
    $(function () {
        $('#enabled2FA').change(function () {
            $('.qrCodeRow').toggleClass("hidden", !this.checked);


            if (this.checked) {
                $.get({
                    url: '/main/qr?commandId=${commandId}',
                    success: function (data) {
                        $('#qrCode').attr('src', data);
                    }
                });

                $('#code').attr("required", true);
            } else {
                $('#code').removeAttr("required");
            }
        });
    });
</script>


</body>

