<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="b" uri="/WEB-INF/tld/customTagLibrary.tld" %>
<body>
<div class="container">
    <div class="col-md-6 login-area shadow-depth-1">
        <fieldset>
            <!-- Form Name -->
            <legend>Author Definition</legend>

            <!-- Text input-->
            <div class="form-group">
                <b:labelValue labelValue="Author Name"
                              value="${author.fullName}"/>

                <b:labelValueList labelValue="Book by author"
                                  list="${author.books}"
                                  valueName="name"/>

                <b:labelValue labelValue="Credits"
                              value="Thanks to GOD"/>

                <b:popoverLink label="More Options"
                               options="${optionMap}"/>
            </div>
        </fieldset>
    </div>
</div>
</body>
