<%@ attribute name="bindPath" required="true" type="java.lang.String" %>
<%@ attribute name="labelValue" required="true" type="java.lang.String" %>
<%@ attribute name="labelSize" required="false" type="java.lang.Integer" %>
<%@ attribute name="valueSize" required="false" type="java.lang.Integer" %>
<%@ attribute name="itemLabel" required="false" type="java.lang.String" %>
<%@ attribute name="itemValue" required="false" type="java.lang.String" %>
<%@ attribute name="optionList" required="true" type="java.util.List" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="labelSize" value="${empty labelSize? 4:labelSize}"/>
<c:set var="valueSize" value="${empty valueSize? 8:valueSize}"/>

<div class="row">
    <div class="col-md-${labelSize}">
        <label class="pull-right">
            <c:out value="${labelValue}"/>
        </label>
    </div>

    <div class="col-md-${valueSize}">
        <c:if test="${not empty optionList}">
            <form:select multiple="true" path="${bindPath}">
                <c:choose>
                    <c:when test="${not empty itemLabel && not empty itemValue}">
                        <form:options items="${optionList}" itemValue="${itemValue}" itemLabel="${itemLabel}"/>
                    </c:when>
                    <c:otherwise>
                        <form:options items="${optionList}"/>
                    </c:otherwise>
                </c:choose>
            </form:select>
        </c:if>
    </div>
</div>
