<%@ attribute name="labelSize" required="false" type="java.lang.Integer" %>
<%@ attribute name="valueSize" required="false" type="java.lang.Integer" %>
<%@ attribute name="labelValue" required="false" type="java.lang.String" %>
<%@ attribute name="list" required="false" type="java.util.Collection" %>
<%@ attribute name="valueName" required="false" type="java.lang.String" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<c:set var="labelSize" value="${empty labelSize? 4:labelSize}"/>
<c:set var="valueSize" value="${empty valueSize? 8:valueSize}"/>

<div class="row">
    <div class="col-md-${labelSize}">
        <label class="pull-right">
            <c:out value="${labelValue}"/>
        </label>
    </div>

    <div class="col-md-${valueSize}">
        <c:if test="${not empty list}">
            <c:forEach var="value" items="${list}" varStatus="counter">
                <label class="col-md-2">
                    <c:out value="${counter.count}"/>.&nbsp;
                </label>

                <label class="col-md-10 pull-left">
                    <c:choose>
                        <c:when test="${not empty valueName}">
                            <c:out value="${value[valueName]}"/>
                        </c:when>
                        <c:otherwise>
                            <c:out value="${value}"/>
                        </c:otherwise>
                    </c:choose>
                </label>
            </c:forEach>
        </c:if>
    </div>
</div>
