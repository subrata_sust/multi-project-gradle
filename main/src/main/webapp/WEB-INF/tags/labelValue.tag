<%@ attribute name="labelSize" required="false" type="java.lang.Integer" %>
<%@ attribute name="labelValue" required="false" type="java.lang.String" %>
<%@ attribute name="valueSize" required="false" type="java.lang.Integer" %>
<%@ attribute name="value" required="false" type="java.lang.String" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<c:set var="labelSize" value="${empty labelSize? 4:labelSize}"/>
<c:set var="valueSize" value="${empty valueSize? 8:valueSize}"/>

<div class="row">
    <div class="col-md-${labelSize}">
        <label class="pull-right">
            <c:out value="${labelValue}"/>
        </label>
    </div>

    <div class="col-md-${valueSize}">
        <label>
            <c:out value="${value}"/>
        </label>
    </div>
</div>
