<%@ attribute name="label" required="true" type="java.lang.String" %>
<%@ attribute name="title" required="false" type="java.lang.String" %>
<%@ attribute name="options" required="true" type="java.util.Map" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<script>
    $(document).ready(function () {
        $('[data-toggle="popover"]').popover({
            html: true,
            viewport: '#container',
            content: $('#content').html()
        });
    });
</script>

<div id="content" class="hidden">
    <c:forEach var="option" items="${options}">
        <div class="row">
            <a href="${option.value}">
                <c:out value="${option.key}"/>
            </a>
        </div>
    </c:forEach>
</div>

<div id="container" class="row">
    <div class="col-md-10">
        <a href="#" title="${title}" data-trigger="focus"  data-toggle="popover">
            <c:out value="${label}"/>
        </a>
    </div>
</div>
